//
//  ViewController.swift
//  Swift_Recipe
//
//  Created by Brad Roring on 9/29/17.
//  Copyright (c) 2017 Brad Roring. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    var myDict: NSArray?
    var selectedObject: Dictionary<String, AnyObject> = [:]
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (myDict?.count)!
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as UITableViewCell
        let obj = myDict?.objectAtIndex(indexPath.row) as Dictionary<String, AnyObject>
        cell.textLabel?.text = obj["Title"] as? String
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedObject = myDict?.objectAtIndex(indexPath.row) as Dictionary<String, AnyObject>
        performSegueWithIdentifier("togo", sender: nil)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let send: SecondView = segue.destinationViewController as SecondView
        send.info = selectedObject
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let tableView = UITableView(frame: self.view.bounds, style: UITableViewStyle.Plain)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tableView)
        
        if let path = NSBundle.mainBundle().pathForResource("Reci", ofType: "plist") {
            myDict = NSArray(contentsOfFile: path)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
