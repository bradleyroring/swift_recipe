//
//  SecondView.swift
//  Swift_Recipe
//
//  Created by Brad Roring on 9/30/17.
//  Copyright (c) 2017 Brad Roring. All rights reserved.
//

import UIKit

class SecondView: UIViewController {

    @IBOutlet weak var TitleLabel: UILabel!
    
    @IBOutlet weak var IngredText: UITextView!
    
    @IBOutlet weak var FoodImage: UIImageView!
    
    @IBOutlet weak var DescipText: UITextView!
    
    var info: Dictionary<String, AnyObject> = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = NSURL(string: (info["Image"]) as String) {
            if let data = NSData(contentsOfURL: url) {
                FoodImage.image = UIImage(data: data)
            }        
        }
        TitleLabel.text = info["Title"] as? String
        IngredText.text = info["Ingredients"] as? String
        DescipText.text = info["Directions"] as? String
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
